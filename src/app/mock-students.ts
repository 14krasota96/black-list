import {Student} from "./student.model";

export const STUDENTS: Student[] = [
  { id: 1001, nameSurname: 'Wes Gibbins'},
  { id: 1002, nameSurname: 'Laurel Castillo' },
  { id: 1003, nameSurname: 'Michaela Pratt' },
  { id: 1004, nameSurname: 'Connor Walsh' },
  { id: 1005, nameSurname: 'Asher Millstone' },
];
