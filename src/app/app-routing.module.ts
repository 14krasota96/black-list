import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent }   from './dashboard/dashboard.component';
import {CourseDetailComponent} from "./course-detail/course-detail.component";
import {CourseComponent} from "./courses/course.component";
import {StudentsDetailComponent} from "./course-detail/students-detail/students-detail.component";

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'detail/:id', component: CourseDetailComponent },
  { path: 'courses', component: CourseComponent },
  { path: 'student/:id', component: StudentsDetailComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
