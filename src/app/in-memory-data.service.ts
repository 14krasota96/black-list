import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Course } from './course';
import { Injectable } from '@angular/core';
import {Student} from "./student.model";

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const courses = [
      { id: 1001, name: 'Course of English'},
      { id: 1002, name: 'Higher Mathematics' },
      { id: 1003, name: 'Power engineering' },
      { id: 1004, name: 'Energy management' },
      { id: 1005, name: 'Course of Philosophy' },
      { id: 1006, name: 'Course of Sociology' },
      { id: 1007, name: 'Course of Criminal law' },
      { id: 1008, name: 'Information Technology' },
      { id: 1009, name: 'Physical education' },
      { id: 1010, name: 'Course of Chemistry' }
    ];

    const students = [
      { id: 1001, nameSurname: 'Wes Gibbins'},
      { id: 1002, nameSurname: 'Laurel Castillo' },
      { id: 1003, nameSurname: 'Michaela Pratt' },
      { id: 1004, nameSurname: 'Connor Walsh' },
      { id: 1005, nameSurname: 'Asher Millstone' },
    ];
    return {courses, students};
  }

  genId(courses: Course[]): number {
    return courses.length > 0 ? Math.max(...courses.map(course => course.id)) + 1 : 1001;
  }
  generateId(students: Student[]): number {
    return students.length > 0 ? Math.max(...students.map(student => student.id)) + 1 : 1001;
  }
}
