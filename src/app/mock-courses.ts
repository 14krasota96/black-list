import { Course } from './course';

export const COURSES: Course[] = [
  { id: 1001, name: 'Course of English'},
  { id: 1002, name: 'Higher Mathematics' },
  { id: 1003, name: 'Power engineering' },
  { id: 1004, name: 'Energy management' },
  { id: 1005, name: 'Course of Philosophy' },
  { id: 1006, name: 'Course of Sociology' },
  { id: 1007, name: 'Course of Criminal law' },
  { id: 1008, name: 'Information Technology' },
  { id: 1009, name: 'Physical education' },
  { id: 1010, name: 'Course of Chemistry' }
];
