import { Component, OnInit } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {Student} from "../../student.model";
import {StudentsService} from "../../students.service";
import {debounceTime, distinctUntilChanged, switchMap} from "rxjs/operators";


@Component({
  selector: 'app-students-serach',
  templateUrl: './students-serach.component.html',
  styleUrls: ['./students-serach.component.css']
})
export class StudentsSerachComponent implements OnInit {

  students$: Observable<Student[]>;
  private searchTerms = new Subject<string>();

  constructor(
    private studentsService: StudentsService
  ) { }

  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.students$ = this.searchTerms.pipe(
      debounceTime(300),

      distinctUntilChanged(),

      switchMap((term: string) => this.studentsService.searchStudents(term)),
    );
  }

}
