import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsSerachComponent } from './students-serach.component';

describe('StudentsSerachComponent', () => {
  let component: StudentsSerachComponent;
  let fixture: ComponentFixture<StudentsSerachComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsSerachComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsSerachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
