import {Component, Input, OnInit} from '@angular/core';
import {StudentsService} from "../../students.service";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import {Student} from "../../student.model";

@Component({
  selector: 'app-students-detail',
  templateUrl: './students-detail.component.html',
  styleUrls: ['./students-detail.component.css']
})
export class StudentsDetailComponent implements OnInit {

  @Input() student: Student;

  constructor(
    private studentsService: StudentsService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.getStudent();
  }

  getStudent(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.studentsService.getStudent(id)
      .subscribe(student => this.student = student);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.studentsService.updateStudent(this.student)
      .subscribe(() => this.goBack());
  }

}
