import { Component, OnInit } from '@angular/core';
import {Student} from "../../student.model";
import {StudentsService} from "../../students.service";

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.css']
})
export class StudentsListComponent implements OnInit {

  students: Student[];

  constructor(
    private studentsService: StudentsService,
  ) {
  }

  ngOnInit() {
    this.getStudent();
  }

  getStudent(): void {
    this.studentsService.getStudents()
      .subscribe(students => this.students = students);
  }

  addStudent(nameSurname: string): void {
    nameSurname = nameSurname.trim();
    if (!nameSurname) {
      return;
    }
    this.studentsService.addStudent({nameSurname} as Student)
      .subscribe(student => {
        this.students.push(student);
      });
  }

  deleteStudent(student: Student): void {
    this.students = this.students.filter(s => s !== student);
    this.studentsService.deleteStudent(student).subscribe();
  }

}
