import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Course } from './course';
import { MessageService } from './message.service';


@Injectable({ providedIn: 'root' })
export class CourseService {

  private coursesUrl = 'api/courses';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  getCourses (): Observable<Course[]> {
    return this.http.get<Course[]>(this.coursesUrl)
      .pipe(
        tap(_ => this.log('fetched courses')),
        catchError(this.handleError<Course[]>('getCourses', []))
      );
  }

  getCourseNo404<Data>(id: number): Observable<Course> {
    const url = `${this.coursesUrl}/?id=${id}`;
    return this.http.get<Course[]>(url)
      .pipe(
        map(courses => courses[0]),
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} course id=${id}`);
        }),
        catchError(this.handleError<Course>(`getCourse id=${id}`))
      );
  }

  getCourse(id: number): Observable<Course> {
    const url = `${this.coursesUrl}/${id}`;
    return this.http.get<Course>(url).pipe(
      tap(_ => this.log(`fetched course id=${id}`)),
      catchError(this.handleError<Course>(`getCourse id=${id}`))
    );
  }

  searchCourses(term: string): Observable<Course[]> {
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<Course[]>(`${this.coursesUrl}/?name=${term}`).pipe(
      tap(_ => this.log(`found courses matching "${term}"`)),
      catchError(this.handleError<Course[]>('searchCourses', []))
    );
  }

  //////// Save methods //////////

  addCourse (course: Course): Observable<Course> {
    return this.http.post<Course>(this.coursesUrl, course, this.httpOptions).pipe(
      tap((newCourse: Course) => this.log(`added course w/ id=${newCourse.id}`)),
      catchError(this.handleError<Course>('addCourse'))
    );
  }

  deleteCourse (course: Course | number): Observable<Course> {
    const id = typeof course === 'number' ? course : course.id;
    const url = `${this.coursesUrl}/${id}`;

    return this.http.delete<Course>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted course id=${id}`)),
      catchError(this.handleError<Course>('deleteCourse'))
    );
  }

  updateCourse (course: Course): Observable<any> {
    return this.http.put(this.coursesUrl, course, this.httpOptions).pipe(
      tap(_ => this.log(`updated hero id=${course.id}`)),
      catchError(this.handleError<any>('updateCourse'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add(`CourseService: ${message}`);
  }
}
