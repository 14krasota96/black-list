import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { CourseSearchComponent } from '../course-search/course-search.component';

import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { COURSES } from '../mock-courses';
import { CourseService } from '../course.service';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let courseService;
  let getCoursesSpy;

  beforeEach(async(() => {
    courseService = jasmine.createSpyObj('courseService', ['getCourse']);
    getCoursesSpy = courseService.getCourse.and.returnValue( of(COURSES) );
    TestBed.configureTestingModule({
      declarations: [
        DashboardComponent,
        CourseSearchComponent
      ],
      imports: [
        RouterTestingModule.withRoutes([])
      ],
      providers: [
        { provide: CourseService, useValue: courseService }
      ]
    })
    .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display "Top Courses" as headline', () => {
    expect(fixture.nativeElement.querySelector('h3').textContent).toEqual('Top Courses');
  });

  it('should call courseService', async(() => {
    expect(getCoursesSpy.calls.any()).toBe(true);
    }));

  it('should display 4 links', async(() => {
    expect(fixture.nativeElement.querySelectorAll('a').length).toEqual(4);
  }));

});
