import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppRoutingModule }     from './app-routing.module';

import { AppComponent }         from './app.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { CourseSearchComponent }  from './course-search/course-search.component';
import { MessagesComponent }    from './messages/messages.component';
import {CourseComponent} from "./courses/course.component";
import {CourseDetailComponent} from "./course-detail/course-detail.component";
import { StudentsListComponent } from './course-detail/students-list/students-list.component';
import { StudentsDetailComponent } from './course-detail/students-detail/students-detail.component';
import { StudentsSerachComponent } from './course-detail/students-serach/students-serach.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,

    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { passThruUnknownUrl: true }
    ),
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    CourseComponent,
    CourseDetailComponent,
    MessagesComponent,
    CourseSearchComponent,
    StudentsListComponent,
    StudentsDetailComponent,
    StudentsSerachComponent
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
